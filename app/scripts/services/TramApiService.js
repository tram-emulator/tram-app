angular.module('tramApp')
    .service('tramAPIService', ['$http', function ($http) {
        const host = 'http://192.168.0.103:8080';

        const makeRequest = function (url) {
            return $http.get(`${host}${url}`)
                .then(
                    response => response.data,
                    error => new Error(`Error in fetching data from api ${url} | ${error}`)
                )
                .then(data => {
                    if (Number(data.status) === 200)
                        return data.response;
                    else
                        throw new Error(`Api error ${data.error.message}`);
                })
        };

        return {
            getTrams: () =>
                makeRequest('/trams/'),
            getStops: (tram_id) =>
                makeRequest(`/stops/?tram_id=${tram_id}`),
            getTramState: (tram_id) =>
                makeRequest(`/get_state/?tram_id=${tram_id}`)
        }
    }]);