angular.module('tramApp').
    controller('tramController', ['$scope', 'tramAPIService', function($scope, tramAPIService) {
        $scope.trams = [];
        $scope.stops = [];
        $scope.active_tram = null;
        $scope.current_stop = null;

        $scope.getStops = function (tram_id) {
            if ($scope.active_tram !== tram_id) {

                $scope.active_tram = tram_id;

                tramAPIService.getStops(tram_id)
                    .then(stops => $scope.stops = stops);

                const ws = new WebSocket(`ws://192.168.0.101:8080/tram_ws/?tram_id=${tram_id}`);

                ws.onmessage = (message) => {
                    let tramState = JSON.parse(JSON.parse(message.data));

                    $scope.$applyAsync(function () {
                        $scope.current_stop = (tramState['stop_id'] !== "None") ? Number(tramState['stop_id']) : null;
                    });

                    let stops = $scope.stops.map(stop => stop.stop_id);

                    if (stops.indexOf($scope.current_stop) === -1)
                        tramAPIService.getStops(tram_id)
                            .then(stops => $scope.stops = stops);
                };
            }
        };

        tramAPIService.getTrams()
            .then(trams => $scope.trams = trams);
    }]);